import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient:HttpClient) { }

  authenticateUser(form:any):Observable<any>{
    return this.httpClient.post('http://localhost:8080/code_igniter/index.php/authenticate/login',form);
  }


}
