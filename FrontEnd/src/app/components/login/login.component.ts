import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _userService:UserService) { }

  ngOnInit() {
  }

  onLogin(form:NgForm){
    console.log(form.form.value);
    this._userService.authenticateUser(form.form.value).subscribe(
      (data)=>{
        console.log(data);
      }
    )
  }

}
