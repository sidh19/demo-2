<?php
defined('BASEPATH') or exit('no direct access to script');
require APPPATH.'helpers/db_helper.php';

class Migrate extends CI_Controller
{

        public function index()
        {
                $this->load->library('migration');

                if ($this->migration->current() === FALSE)
                {
                        show_error($this->migration->error_string());
                }
                else{
                    echo 'success!!';
                }
        }

}