<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';
require_once APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/Format.php';
require_once APPPATH . 'libraries/bitbucket_api.php';
require_once APPPATH . 'libraries/sing.php';

use kamermans\OAuth2\GrantType\AuthorizationCode;
use kamermans\OAuth2\GrantType\RefreshToken;
use kamermans\OAuth2\Persistence\FileTokenPersistence;
use kamermans\OAuth2\OAuth2Middleware;
use kamermans\OAuth2\Token\RawToken;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;
use kamermans\OAuth2\GrantType\ClientCredentials;
use Restserver\Libraries\REST_Controller;
use kamermans\OAuth2\Exception\OAuth2Exception;
use function GuzzleHttp\json_decode;
use Doctrine\Common\Cache\MemcachedCache;
use Cache\Adapter\Doctrine\DoctrineCachePool;
use kamermans\OAuth2\Signer\AccessToken\BearerAuth;
use GuzzleHttp\Psr7\Request;
use Demo\First\Sing;

class Authenticate extends CI_Controller
{

    use REST_Controller {
    REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {

        parent::__construct();
		$this->__resTraitConstruct();
		
    }
    public function index()
    {
        echo 'Authentication page';
    }

    public function login_post()
    {
        $username = $this->post('username');
        $password = $this->post('password');
		
        try {
            $query = $this->db->query('select * from users where email=?', [$username]);
            $user = $query->row();
            if (isset($user)) {
                if (password_verify($password, $user->password)) {
                    // Create a token from the user data and send it as reponse
                    $client_token = AUTHORIZATION::generateToken(['username' => $username]);
                    $client_id = 'p5Yn4v8YTBf3wjbA45';
					$client_secret = 'p8JqmUJeE45Pv74b8cEy35nhxgmS3T2D';
					
					Bitbucket_api::AuthenticateClient($client_id, $client_secret,$user);
					
					$bb_jwt=Bitbucket_api::getAccessToken();
					$this->db->set('bb_jwt', $bb_jwt);
					$this->db->where('id', $user->id);
					$this->db->update('users');
                    $this->response($bb_jwt ,200);

                } else {
                    $this->response(['error' => 'invalid password'], 401);
                }
            } else {
                $this->response(['error' => 'invalid email'], 401);
            }
        } catch (\Throwable $th) {
            if($th instanceof OAuth2Exception){
                $this->response(['error' => 'Bitbucket auth failed'], 401);
            }
        }
    }

    public function get_repos_get(){
		$username='user-1@mail.com';
		$query = $this->db->query('select * from users where email=?', [$username]);
        $user = $query->row();
		$request = new Request('GET', 'https://api.bitbucket.org/2.0/repositories/{'.$user->uuid.'}');
        /* $signer = new BearerAuth();
		$signer->sign($request,$user->bb_jwt); */
		
        $client = new Client(['headers' => ['Authorization' => 'Bearer {'.$user->bb_jwt.'}']]);
		$response = $client->send($request);
		
		//$response = Bitbucket_api::get_all_repositories('1da35caa-338a-4e8b-84d3-6a2233e8808e');
        
        // $resp=$client->request('GET', 'https://api.bitbucket.org/2.0/repositories/{'.$uuid_or_username.'}');
       
        $this->response(json_decode( $response->getBody() ) );
    }
}
