<?php
namespace Demo\First;

defined('BASEPATH') OR exit('No direct script access allowed');
class Sing{
	private static $singleton=null;
	private $a=null;
	private function __construct(){
		$this->a=10;
		echo 'construcotr';
	}

	public static function getInstance(){
		if(self::$singleton===null){
			self::$singleton=new Sing();
		}
		return self::$singleton;
	}

	public function getA(){
		return $this->a;
	}
}
