<?php


use kamermans\OAuth2\OAuth2Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;
use kamermans\OAuth2\GrantType\ClientCredentials;
use kamermans\OAuth2\Persistence\DoctrineCacheTokenPersistence;



require_once FCPATH . 'vendor/autoload.php';

class Bitbucket_api
{
    protected static $auth_client;
    protected static $client = null;
    protected static $oauth = null;
    //protected static $stack = null;
    //protected $uuid_or_username;


    public static function AuthenticateClient(string $client_id, string $client_secret, $user){
        if( self::$client == null){
            
            self::$auth_client = new Client([
                'base_uri' => 'https://bitbucket.org/site/oauth2/access_token'
            ]);
			$grant_type = new ClientCredentials( self::$auth_client, ['client_id' => $client_id,
			 'client_secret' => $client_secret]);
			self::$oauth = new OAuth2Middleware( $grant_type );
			$token_path = APPPATH.'/bb_tokens/access_token.json';
			$token_persistence = new DoctrineCacheTokenPersistence(  new Doctrine\Common\Cache\ChainCache(),'1da35caa-338a-4e8b-84d3-6a2233e8808e');
			self::$oauth->setTokenPersistence($token_persistence);
            $stack = HandlerStack::create();
            $stack->push(self::$oauth);  
            self::$client = new Client([
                'handler' => $stack,
                'auth' => 'oauth',
			]);
			$CI= & get_instance();
			$CI->load->driver('cache',
        			array('adapter' => 'apc', 'backup' => 'file')
				);
			
			$CI->cache->save('111','hi');
			
			$tok=$CI->cache->get('111');
			echo $tok;
			
        }
        return self::$client;
    }

    public static function getAuthenticatedClient(){
        return self::$client;
    }

    public static  function getAccessToken(){
       return self::$oauth->getAccessToken();
    }
  
    public static function get_all_repositories(string $uuid_or_username)
    {
        $CI = & get_instance();
        $client=self::getAuthenticatedClient();
        $response = $client->request('GET', 'https://api.bitbucket.org/2.0/repositories/{'.$uuid_or_username.'}');
        
        echo self::$oauth->getAccessToken();
        
        return $response;
    }
}
